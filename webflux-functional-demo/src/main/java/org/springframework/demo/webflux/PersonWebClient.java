package org.springframework.demo.webflux;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import java.util.Random;
import java.util.stream.Collectors;

public class PersonWebClient {
    private WebClient client = WebClient.create("http://localhost:8080");
    private Flux<Person> people = client.get()
            .uri("/person")
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .bodyToFlux(Person.class);

    public static void main(String[] args) {
        PersonWebClient gwc = new PersonWebClient();
        gwc.addPerson(new Person("Adam", new Random().nextInt(100)));
        System.out.println(gwc.getResult());
    }

    private String getResult() {
        //block below is blocking
        return ">> people = " + people.collectList().block().stream().map(Person::toString).collect(Collectors.joining());
    }

    private String addPerson(Person person) {
        return client.post()
                .uri("/person/")
                .body(BodyInserters.fromObject(person))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .retrieve()
                .bodyToMono(String.class)
                .block();
    }
}