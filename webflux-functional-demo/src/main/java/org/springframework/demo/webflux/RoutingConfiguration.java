package org.springframework.demo.webflux;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.config.EnableWebFlux;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.BodyInserters.fromObject;
import static org.springframework.web.reactive.function.server.RequestPredicates.*;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
@EnableWebFlux
public class RoutingConfiguration {

    @Bean
    public PersonRepository repository() {
        return new PersonRepositoryImpl();
    }

    @Bean
    public PersonHandler handler(PersonRepository repository) {
        return new PersonHandler(repository);
    }

    @Bean
    public RouterFunction<ServerResponse> routerFunction(PersonHandler handler) {
        return route(GET("/person"), handler::allPeople)
                .andRoute(GET("/person/{id}").and(accept(APPLICATION_JSON)), handler::getPerson)
                .andRoute(POST("/person").and(contentType(APPLICATION_JSON)), handler::savePerson);
//				return nest(path("/person"),
//				nest(accept(APPLICATION_JSON),
//						route(GET("/{id}"), handler::getPerson)
//								.andRoute(method(HttpMethod.GET), handler::allPeople)
//				).andRoute(POST("/").and(contentType(APPLICATION_JSON)), handler::savePerson));
    }

    @Bean
    public RouterFunction<ServerResponse> otherFunction() {
        return route(GET("/otherFunction"), (a) -> ServerResponse.ok().body(fromObject("otherFunction")));
    }
}
